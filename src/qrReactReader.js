import React, { useState } from 'react';
import { QrReader } from 'react-qr-reader';


const TestQr = (props) => {
    const [data, setData] = useState('No result');
  
    return (
      <>
        <QrReader
          constraints={{facingMode: 'environment'}}
          onResult={(result, error) => {
            if (!!result) {
              setData(result?.text);
            }
  
            if (!!error) {
              console.log(error);
            }
          }}
          style={{ width: '100%' }}
        />
        <p>{data}</p>
      </>
    );
  };

export default TestQr;